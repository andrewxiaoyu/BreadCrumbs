
  require([
    "esri/Color",
    "esri/geometry/Point",
    "esri/geometry/webMercatorUtils",
    "esri/graphic",
    "esri/layers/FeatureLayer",
    "esri/map",
    "esri/renderers/SimpleRenderer",
    "esri/renderers/TemporalRenderer",
    "esri/renderers/TimeClassBreaksAger",
    "esri/symbols/SimpleLineSymbol",
    "esri/symbols/SimpleMarkerSymbol",
    "esri/TimeExtent",
    "dojo/domReady!",
    "dojox/socket"
  ], function (Color, Point, webMercatorUtils, Graphic, FeatureLayer, Map, SimpleRenderer, TemporalRenderer,
    TimeClassBreaksAger, SimpleLineSymbol, SimpleMarkerSymbol, TimeExtent, Socket){

    var map, featureLayer;
    // var OBJECTID_COUNTER = 1000;
    // var TRACKID_COUNTER = 1;
    // //onorientationchange doesn't always fire in a timely manner in Android so check for both orientationchange and resize
    // var supportsOrientationChange = "onorientationchange" in window, orientationEvent = supportsOrientationChange ? "orientationchange" : "resize";

    // window.addEventListener(orientationEvent, function (){
    //   orientationChanged();
    // }, false);

    map = new Map("map", {
      basemap: "streets",
      zoom:6

    });
    var socket;
    var serverData;
    var parsedJsonEx={};//={"52,3":{grabTime:"52t", item:"3t"},"43,6":{grabTime:"43t", item:"6t"},"6,5":{grabTime:"", item:""},
    // "3,1":{grabTime:"", item:""},"23,5":{grabTime:"", item:""},
    // "34,75":{grabTime:"", item:""},"37,1":{grabTime:"", item:""},"2,42":{grabTime:"", item:""}}; //read in every 2
    var convertedJson={};
    var coordList=[];
    //temp vars
    var parsedJsonEx2={};
    var coordList2 = [];//= ["52","3","43","6","6","5","3","1","23","5","34","75","37","1","2","42"]
    map.on("load", function(){
        console.log("done loading map");
      
        var args, ws = typeof WebSocket != 'undefined';
        console.log('ayy')
        window.fetch('/sockets-are-bad/').then(function (res) {
                return res.json();
            }).then(function (data) {
                console.log(data)
                serverData = data.events;
                console.log(serverData)
                for(var i=0;i<serverData.length;i+=1){
                    createKey=serverData[i].lat+","+serverData[i].lng;
                    parsedJsonEx[createKey]={hold:serverData[i].hold, time:serverData[i].time};
                    coordList.push(serverData[i].lat,serverData[i].lng);
                }
                console.log("dictionary")
                console.log(parsedJsonEx);
                mapLoadedHandler(true);
            });
        window.setInterval(function () {
            window.fetch('/sockets-are-bad/').then(function (res) {
                return res.json();
            }).then(function (data) {
                console.log(data)
                serverData = data.events;
                console.log(serverData)
                
                for(var i=0;i<serverData.length;i+=1){
                    coordList2.push(serverData[i].lat,serverData[i].lng);
                }
                //updates
                if(coordList!=coordList2){
                    for(var i=0;i<serverData.length;i+=1){
                        createKey=serverData[i].lat+","+serverData[i].lng;
                        parsedJsonEx2[createKey]={hold:serverData[i].hold, time:serverData[i].time};
                        if(i==serverData.length-1){
                            if(!(createKey in parsedJsonEx)){
                                var marker;
                                if(serverData[i].hold=="True")
                                    marker=new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 10,
                                    new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                                    new Color([0,0,255]), 1),
                                    new Color([0,0,255,0.25])); 
                                else{
                                    marker=new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 10,
                                    new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                                    new Color([255,0,0,]), 1),
                                    new Color([255,0,0,,0.25]));
                                }
                                var newPt = webMercatorUtils.geographicToWebMercator(new Point(jsonObj[i+1],jsonObj[i]));
                                var key = newPt.x+","+newPt.y;
                                convertedJson[key]=createKey;
                                console.log(newPt)
                                console.log("urbad")
                                var graphic = new Graphic(newPt, marker);
                                map.graphics.add(graphic);
                                console.log("added point on map")
                                coordList=coordList2
                                parsedJsonEx=parsedJsonEx2
                                addList(parsedJsonEx)
                            }
                        }
                    }
                    
                    
                }
                
                
                
                
                console.log("dictionary")
                console.log(parsedJsonEx);
                mapLoadedHandler(false);
            });
        }, 30 * 1000);

        
        });



    function mapLoadedHandler(initCall){

      //console.log("map loaded", maploadEvent);

      //create a layer definition for the gps points
      var layerDefinition = {
        "objectIdField": "OBJECTID",
        "trackIdField": "TrackID",
        "geometryType": "esriGeometryPoint",
        "timeInfo": {
          "startTimeField": "DATETIME",
          "endTimeField": null,
          "timeExtent": [1277412330365],
          "timeInterval": 1,
          "timeIntervalUnits": "esriTimeUnitsMinutes"
        },
        "fields": [
          {
            "name": "OBJECTID",
            "type": "esriFieldTypeOID",
            "alias": "OBJECTID",
            "sqlType": "sqlTypeOther"
          },
          {
            "name": "TrackID",
            "type": "esriFieldTypeInteger",
            "alias": "TrackID"
          },
          {
            "name": "DATETIME",
            "type": "esriFieldTypeDate",
            "alias": "DATETIME"
          }
        ]
      };

      var featureCollection = {
        layerDefinition: layerDefinition,
        featureSet: null
      };
      featureLayer = new FeatureLayer(featureCollection);

      //setup a temporal renderer
      var sms = new SimpleMarkerSymbol().setColor(new Color([255, 0, 0])).setSize(8);
      var observationRenderer = new SimpleRenderer(sms);
      var latestObservationRenderer = new SimpleRenderer(new SimpleMarkerSymbol());
      var infos = [
        {
          minAge: 0,
          maxAge: 1,
          color: new Color([255, 0, 0])
        }, {
          minAge: 1,
          maxAge: 5,
          color: new Color([255, 153, 0])
        }, {
          minAge: 5,
          maxAge: 10,
          color: new Color([255, 204, 0])
        }, {
          minAge: 10,
          maxAge: Infinity,
          color: new Color([0, 0, 0, 0])
        }
      ];
      var ager = new TimeClassBreaksAger(infos, TimeClassBreaksAger.UNIT_MINUTES);
      var sls = new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
        new Color([255, 0, 0]), 3);
      var trackRenderer = new SimpleRenderer(sls);
      var renderer = new TemporalRenderer(observationRenderer, latestObservationRenderer,
        trackRenderer, ager);
      featureLayer.setRenderer(renderer);
      map.addLayer(featureLayer);
      console.log("kys1")
      
      console.log("ky2s")
      if(initCall){
          readJson(coordList) //initial zoom
          addPoints(coordList)
          listenerOn()
          addList(parsedJsonEx)
      }
      
      
        //navigator.geolocation.watchPosition(showLocation, locationError);
        
      
      console.log("ky4s")
    }
    
    console.log("ky5s")

    
    
    
    
    
    function locationError(error){
      switch (error.code) {
        case error.PERMISSION_DENIED:
          alert("Location not provided");
          break;

        case error.POSITION_UNAVAILABLE:
          alert("Current location not available");
          break;

        case error.TIMEOUT:
          alert("Timeout");
          break;

        default:
          alert("unknown error");
          break;
      }
    }

    function initialZoom(x,y){
        var initPt = webMercatorUtils.geographicToWebMercator(new Point(y,x));
        map.centerAndZoom(initPt,9)
    }
    
    function addList(itemDict){
        document.getElementById("list").innerHTML=""
        var counter=0;
        var listEA=[]
        for(item in itemDict){
            document.getElementById("list").innerHTML +="<li id=\"listItem"+counter+"\" class=\"clickable\" >"+
                    "<div class=\"flex-container\">"+
                      "<div class=\"coordinates\">("+item+")</div>"+
                      "<div class=\"time\">"+itemDict[item].time+"</div>"+
                    "</div>"+
                "</li>"
            
            counter+=1
        }
        dojo.connect(dojo.byId("list"),'onclick',linked);
    }
    function linked(listEle){
        listEle = listEle || window.event
        listEle = listEle.path[2]
        console.log(listEle.children[0])
        console.log(listEle.children[0].children[0].innerHTML)
        str=listEle.children[0].children[0].innerHTML
        key=str.substring(1,str.length-1)
        
        var garray=map.graphics.graphics  
        console.log(garray)
        var big=(obj.graphic.symbol.size===30)
        console.log(big)
        for (var i = garray.length - 1; i >= 1; i--) {
            accessKey=garray[i].geometry.x+","+garray[i].geometry.y;
            if(parsedJsonEx[convertedJson[accessKey]].hold=="True"){
                garray[i].setSymbol(new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 10,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color([0,0,255]), 1),
                new Color([0,0,255,0.25])))
            }
            else{
                garray[i].setSymbol(new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 10,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color([255,0,0]), 1),
                new Color([255,0,0,0.25])))
            }
        }
        if(!big){
            pointC=obj.graphic
            if(parsedJsonEx[convertedJson[accessKey]].hold=="True"){
                pointC.setSymbol(new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 30,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color([0,0,255]), 1),
                new Color([0,0,255,0.25])))
                document.getElementById("info").innerHTML ="<h1>("+convertedJson[accessKey]+")</h1><p>Time Picked Up : "+parsedJsonEx[convertedJson[accessKey]].time+"</p>"
            }
            else{
                pointC.setSymbol(new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 30,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color([255,0,0]), 1),
                new Color([255,0,0,0.25])))
                document.getElementById("info").innerHTML ="<h1>("+convertedJson[accessKey]+")</h1><p>Time Dropped : "+parsedJsonEx[convertedJson[accessKey]].time+"</p>"
            }
        }
    }
    function addPoints(jsonObj){
        console.log('in addpoints')
      //var pt = webMercatorUtils.geographicToWebMercator(new Point(location.coords.longitude,
      //location.coords.latitude));
      //jsonObj=readJson(jsonObj);
        console.log('finished reading')
        var marker = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 10,
        new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
        new Color([255,0,0]), 1),
        new Color([0,255,0,0.25]));
        console.log('before for loop')
        for(var i=0;i<jsonObj.length;i+=2){
            console.log("zoom")
            var pt2 = webMercatorUtils.geographicToWebMercator(new Point(jsonObj[i+1],jsonObj[i]));
            var key = pt2.x+","+pt2.y;
            convertedJson[key]=jsonObj[i]+","+jsonObj[i+1];
            console.log(pt2)
            console.log("urbad")
            var graphic = new Graphic(pt2, new SimpleMarkerSymbol());
            map.graphics.add(graphic);
            console.log("added point on map")
        }
    }
    
    function listenerOn(){
        console.log("urbsad")
        map.graphics.on("click",pointclicked)
        map.graphics.on("mouse-move",pointhovered)
    }

    function readJson(jsonObj){
      // var obj=JSON.parse(this.responseText);
      //creating data structure from json



      //-------------
        console.log('reading json')
        if(jsonObj.length>1){
            console.log('initializing zoom')
            initialZoom(jsonObj[0],jsonObj[1])
            console.log("finished zooming")
        }
        console.log('returning parsed json obj')
        return jsonObj;
    }



    function pointhovered(event){
        console.log("click the point!")
    }
    function pointclicked(obj){
        console.log(obj)

        var garray=map.graphics.graphics  
        console.log(garray)
        var big=(obj.graphic.symbol.size===30)
        console.log(big)
        for (var i = garray.length - 1; i >= 1; i--) {
            accessKey=garray[i].geometry.x+","+garray[i].geometry.y;
            if(parsedJsonEx[convertedJson[accessKey]].hold=="True"){
                garray[i].setSymbol(new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 10,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color([0,0,255]), 1),
                new Color([0,0,255,0.25])))
            }
            else{
                garray[i].setSymbol(new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 10,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color([255,0,0]), 1),
                new Color([255,0,0,0.25])))
            }
        }
        document.getElementById("info").innerHTML = "";
        if(!big){
            pointC=obj.graphic
            accessKey=pointC.geometry.x+","+pointC.geometry.y;
            if(parsedJsonEx[convertedJson[accessKey]].hold=="True"){
                pointC.setSymbol(new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 30,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color([0,0,255]), 1),
                new Color([0,0,255,0.25])))
                document.getElementById("info").innerHTML ="<h1>("+convertedJson[accessKey]+")</h1><p>Time Picked Up : "+parsedJsonEx[convertedJson[accessKey]].time+"</p>"
            }
            else{
                pointC.setSymbol(new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 30,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                new Color([255,0,0]), 1),
                new Color([255,0,0,0.25])))
                document.getElementById("info").innerHTML ="<h1>("+convertedJson[accessKey]+")</h1><p>Time Dropped : "+parsedJsonEx[convertedJson[accessKey]].time+"</p>"
    
            }
        }

    }

    

// function orientationChanged(){
//       if (map) {
//         map.reposition();
//         map.resize();
//       }
//     }
   

   

  });

