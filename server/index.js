var express = require('express');
var app = express();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var port = process.env.PORT || 3000;
var everything={events:[]};
var path = require('path');
// var ws = require('ws');

// const wss = new ws.Server({server: http});


app.use(express.json());       // to support JSON-encoded bodies
app.use(express.urlencoded()); // to support URL-encoded bodies
app.use('/js', express.static(path.join(__dirname, 'js')));
app.use('/css', express.static(path.join(__dirname, 'css')));
// app.use((req, res) => {res.sendFile(__dirname + '/map.html')});


// wss.broadcast = function (event, data) {
//     wss.clients.forEach((client) => {
//         if (client.readyState === ws.OPEN) {
//             message = {
//                 'event': event,
//                 'data': data
//             };
//             client.send(JSON.stringify(message));
//         }
//     });
// };

app.get('/', function(req, res){
    res.sendFile(__dirname + '/map.html');
});

app.get('/sockets-are-bad/', function (req, res) {
    console.log('----------------------------');
    console.log('Received GET to /sockets-are-bad/');
    console.log('----------------------------');
    res.send(everything);
});

app.post('/cannotcan/', function (req, res) {
    console.log('----------------------------');
    console.log('Received POST to /cannotcan/');
    console.log(req.body);
    console.log('----------------------------');
    io.emit('statusChange', { hold: req.body.hold, time: req.body.time });
    res.send('Thanks for pushing lol');
});

app.post('/coords/', function (req, res) {
    console.log('-------------------------');
    console.log('Received POST to /coords/');
    console.log(req.body);
    console.log('-------------------------');
    var lat = req.body.lat;
    var lng = req.body.lng;
    var hold = req.body.hold;
    var time = req.body.time;
    everything.events.push({
        lat: lat,
        lng: lng,
        hold: hold,
        time: time
    });
    // console.log('Updated everything:', everything);
    io.emit('mapUpdate', everything);
    res.send('Thanks for pushing lol');
});

io.on('connection', function(socket) {
  console.log('A user connected');
  console.log(everything);
  //Whenever someone disconnects this piece of code executed
  socket.on('close', function () {
      console.log('A user disconnected');
  });
  io.emit('mapUpdate', everything);
});

http.listen(port, function(){
  console.log('listening on *:' + port);
});